---
attachments: [Fysisk-diagram.png, Logisk-diagram.png]
title: Netværks- og kommonikationssikkerhed - Opgave 8
created: '2023-02-26T20:12:33.608Z'
modified: '2023-02-26T20:50:31.215Z'
---

# Netværks- og kommonikationssikkerhed - Opgave 8

## Formål
Formålet med denne opgave er at få et værktøj til at lave overskuelige netværksdiagrammer samt at få øvet hvordan de tegnes. Formålet er også at få repeteret fundamentale begreber som IP adresser, VLAN's og forskellen på fysiske og logiske diagrammer.

1. Ovenstående diagram skal laves pænt - ie. find et passende diagram værktøj, og lav Jeres egen version af det.
2. Tilføj noget associeret tekst der beskriver hvad man ser og de vigtige elementer
3. Find 3 spørgsmål til diagrammet og tag dem med til næste undervisningsgang - Det er ok at have fundet svaret på forhånd, så skal du bare notere spørgsmål+svar (inkl. referencer).

### Spg. 1
Følgende er lavet med draw.io

- Fysisk diagram

![Fysisk-diagram](../NetSikkerhed/attachments/Fysisk-diagram.png)

Et fysisk netværksdiagram viser den faktiske fysiske struktur af et netværk, inklusive enhederne, kablerne og deres fysiske placering i forhold til hinanden. Dette diagram giver et visuelt billede af, hvordan enhederne er tilsluttet og kommunikerer med hinanden i netværket. Et fysisk netværksdiagram er normalt mere anvendeligt for teknikere og IT-folk, der er ansvarlige for installation og vedligeholdelse af netværket.

- Logisk-diagram

![Logisk-diagram](../NetSikkerhed/attachments/Logisk-diagram.png)

Et logisk netværksdiagram fokuserer på netværkets logiske struktur, herunder IP-adresser, routingprotokoller, virtuelle LAN (VLAN), firewall-regler og andre konfigurationer, der er nødvendige for at forstå netværkets funktion. Dette diagram er normalt mere anvendeligt for netværksarkitekter og andre beslutningstagere, der har brug for et overblik over netværkets design og funktionalitet.

Kort sagt, mens et fysisk netværksdiagram viser den fysiske infrastruktur, der udgør netværket, viser et logisk netværksdiagram netværkets logiske struktur og hvordan datastrømmen og kommunikationen mellem enhederne på netværket fungerer.

### Spg. 3
- Hvordan bør man navngive i et fysisk diagram? Er der nogle regler/standarter.

- Hvor i netværket giver det mest mening af placerer sin firewall?


